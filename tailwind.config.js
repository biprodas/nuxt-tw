module.exports = {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
  ],
  // darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        "bookmark-purple": "#5267DF",
        "bookmark-red": "#FA5959",
        "bookmark-blue": "#242A45",
        "bookmark-grey": "#9194A2",
        "bookmark-white": "#F7F7F7",
      }
    },
    // fontFamily: {
    //   Poppins: ["Poppins, sans-serif"]
    // },
    letterSpacing: {
      tightest: '-.1em',
      tighter: '-.05em',
      tight: '-.025em',
      normal: '0',
      wide: '.025em',
      wider: '.05em',
      widest: '.1em',
      widest: '.25em',
    },
    container: {
      center: true,
      padding: "1rem",
      // screen: {
      //   lg: "1124px",
      //   xl: "1124px",
      //   "2xl": "1124px"
      // }
    },
  },
  plugins: [],
}
